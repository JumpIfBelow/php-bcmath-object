<?php

namespace JumpIfBelow\BCMath;

use JumpIfBelow\BCMath\Exception\NonNumericValueException;
use JumpIfBelow\BCMath\Exception\OutOfBoundNumberException;

class BCMath
{
    protected string $value;
    protected int $scale;

    protected function __construct(string $value, int $scale)
    {
        $this->value = $value;
        $this->scale = $scale;
    }

    /**
     * @param string $value
     * @param int|null $scale
     * @return static
     * @throws NonNumericValueException
     */
    public static function from(string $value, ?int $scale = null): self
    {
        if (!preg_match('/^[+-]?\d+(?:\.\d+)?$/', $value)) {
            throw new NonNumericValueException();
        }

        return new static($value, $scale ?? '0');
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * @return int
     * @throws OutOfBoundNumberException
     */
    public function toInt(): int
    {
        if (($int = filter_var($this->value, FILTER_VALIDATE_INT)) === false) {
            throw new OutOfBoundNumberException(
                sprintf(
                    'Trying to convert %s into integer, but only numbers between %s and %s without floating point could be converted.',
                    $this->value,
                    PHP_INT_MIN,
                    PHP_INT_MAX
                )
            );
        }

        return $int;
    }

    /**
     * @return float
     * @throws OutOfBoundNumberException
     */
    public function toFloat(): float
    {
        if (($float = filter_var($this->value, FILTER_VALIDATE_FLOAT)) === false) {
            throw new OutOfBoundNumberException(
                sprintf(
                    'Trying to convert %s into float, but only numbers between %s and %s could be converted.',
                    $this->value,
                    PHP_FLOAT_MIN,
                    PHP_FLOAT_MAX
                )
            );
        }

        return $float;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function getScale(): int
    {
        return $this->scale;
    }

    /**
     * @param string $value
     * @return static
     */
    public function withValue(string $value): self
    {
        return static::from($value, $this->scale);
    }

    /**
     * @param int $scale
     * @return static
     */
    public function withScale(int $scale): self
    {
        return static::from($this->value, $scale);
    }

    /**
     * @param string $rightOperand
     * @param int|null $scale
     * @return static
     * @see bcadd()
     */
    public function add(string $rightOperand, ?int $scale = null): self
    {
        return $this->doOperation('bcadd', $rightOperand, $scale);
    }

    /**
     * @param string $rightOperand
     * @param int|null $scale
     * @return int
     * @see bccomp()
     */
    public function comp(string $rightOperand, ?int $scale = null): int
    {
        $scale = $scale ?? $this->scale;

        return bccomp($this->value,  $rightOperand, $scale);
    }

    /**
     * @param string $rightOperand
     * @param int|null $scale
     * @return static
     * @see bcdiv()
     */
    public function div(string $rightOperand, ?int $scale = null): self
    {
        return $this->doOperation('bcdiv', $rightOperand, $scale);
    }

    /**
     * @param string $rightOperand
     * @param int|null $scale
     * @return static
     * @see bcmod()
     */
    public function mod(string $rightOperand, ?int $scale = null): self
    {
        return $this->doOperation('bcmod', $rightOperand, $scale);
    }

    /**
     * @param string $rightOperand
     * @param int|null $scale
     * @return static
     * @see bcmul()
     */
    public function mul(string $rightOperand, ?int $scale = null): self
    {
        return $this->doOperation('bcmul', $rightOperand, $scale);
    }

    /**
     * @param string $rightOperand
     * @param int|null $scale
     * @return static
     * @see bcpow()
     */
    public function pow(string $rightOperand, ?int $scale = null): self
    {
        return $this->doOperation('bcpow', $rightOperand, $scale);
    }

    /**
     * @param string $exponent
     * @param string $modulus
     * @param int|null $scale
     * @return static
     * @see bcpowmod()
     */
    public function powmod(string $exponent, string $modulus, ?int $scale = null): self
    {
        $scale = $this->getOperationScale($scale);

        return static::from(bcpowmod($this->value, $exponent, $modulus, $scale), $scale);
    }

    /**
     * @param int|null $scale
     * @return static
     * @see bcsqrt()
     */
    public function sqrt(?int $scale = null): self
    {
        $scale = $this->getOperationScale($scale);

        return static::from(bcsqrt($this->value, $scale), $scale);
    }

    /**
     * @param string $rightOperand
     * @param int|null $scale
     * @return static
     * @see bcsub()
     */
    public function sub(string $rightOperand, ?int $scale = null): self
    {
        return $this->doOperation('bcsub', $rightOperand, $scale);
    }

    /**
     * A custom-implemented floor function for BCMath.
     * @return static
     * @see floor()
     */
    public function floor(): self
    {
        $toAdd = $this->comp(0) >= 0 ? '0' : '-1';

        return static::from(
            $this->add($toAdd, 0),
            $this->scale,
        );
    }

    /**
     * A custom-implemented ceil function for BCMath.
     * @return static
     * @see ceil()
     */
    public function ceil(): self
    {
        $toAdd = $this->comp(0) >= 0 ? '1' : '0';

        return static::from(
            $this->add($toAdd, 0),
            $this->scale,
        );
    }

    /**
     * A custom-implemented round function for BCMath.
     * @param int $precision The number of decimals to keep.
     * @param int $mode One of {@see PHP_ROUND_HALF_UP}, {@see PHP_ROUND_HALF_DOWN}, {@see PHP_ROUND_HALF_EVEN} or {@see PHP_ROUND_HALF_ODD}.
     * @return static
     * @see round()
     */
    public function round(int $precision = 0, int $mode = PHP_ROUND_HALF_UP): self
    {
        return static::from(
            round($this->value, $precision, $mode),
            $this->scale,
        );
    }

    /**
     * @param callable $operation
     * @param string $rightOperand
     * @param int|null $scale
     * @return static
     */
    protected function doOperation(callable $operation, string $rightOperand, ?int $scale): self
    {
        $scale = $this->getOperationScale($scale);

        return static::from($operation($this->value,  $rightOperand, $scale), $scale);
    }

    /**
     * @param int|null $scale
     * @return int
     */
    protected function getOperationScale(?int $scale): int
    {
        return $scale ?? $this->scale;
    }
}
