<?php

namespace Tests\JumpIfBelow\BCMath;

use JumpIfBelow\BCMath\BCMath;
use JumpIfBelow\BCMath\Exception\NonNumericValueException;
use JumpIfBelow\BCMath\Exception\OutOfBoundNumberException;
use PHPUnit\Framework\TestCase;

class BCMathTest extends TestCase
{
    public function testFrom(): void
    {
        $b = BCMath::from(1);

        $this->assertEquals('1', $b->getValue());
        $this->assertEquals('0', $b->getScale());

        $b = BCMath::from(50, 10);

        $this->assertEquals('50', $b->getValue());
        $this->assertEquals('10', $b->getScale());

        $b = BCMath::from('+50');
        $this->assertEquals('+50', $b->getValue());
        $this->assertEquals('51', $b->add('1')->getValue());
    }

    public function test__toString(): void
    {
        $b = BCMath::from(0);

        $this->assertEquals('0', (string) $b);
    }

    public function testToInt(): void
    {
        $b = BCMath::from(0);

        $this->assertEquals(0, $b->toInt());

        $b = BCMath::from(PHP_INT_MAX);

        $this->assertEquals(PHP_INT_MAX, $b->toInt());

        $b = BCMath::from(PHP_INT_MAX)->add(1);

        $this->expectExceptionObject(new OutOfBoundNumberException('Trying to convert 9223372036854775808 into integer, but only numbers between -9223372036854775808 and 9223372036854775807 without floating point could be converted.'));
        $b->toInt();
    }

    public function testToFloat(): void
    {
        $b = BCMath::from(0.5);

        $this->assertEquals(0.5, $b->toFloat());

        $this->expectExceptionObject(new NonNumericValueException());
        BCMath::from(PHP_FLOAT_MAX);

        $b = BCMath::from(10)->pow(309);

        $this->expectExceptionObject(new OutOfBoundNumberException('Trying to convert 1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 into float, but only numbers between 2.2250738585072E-308 and 1.7976931348623E+308 could be converted.'));
        $b->toFloat();
    }

    public function testGetValue(): void
    {
        $b = BCMath::from('5');

        $this->assertEquals('5', $b->getValue());

        $b = BCMath::from('58');

        $this->assertEquals('58', $b->getValue());
    }

    public function testGetScale(): void
    {
        $b = BCMath::from('5');

        $this->assertEquals(0, $b->getScale());

        $b = BCMath::from('5', 2);

        $this->assertEquals(2, $b->getScale());
    }

    public function testWithValue(): void
    {
        $b = BCMath::from('0');

        $this->assertNotSame($b, $b->withValue('5'));

        $b = $b->withValue('5');
        $this->assertEquals('5', $b->getValue());

        $b->withValue('0');
        $this->assertEquals('5', $b->getValue());
    }

    public function testWithScale(): void
    {
        $b = BCMath::from('0');

        $this->assertNotSame($b, $b->withScale(5));

        $b = $b->withScale(5);
        $this->assertEquals(5, $b->getScale());

        $b->withScale(0);
        $this->assertEquals(5, $b->getScale());
    }

    public function testAdd(): void
    {
        $b = BCMath::from('4');

        $this->assertNotSame($b, $b->add('0'));
        $this->assertEquals('10', $b->add('6')->getValue());
    }

    public function testComp(): void
    {
        $b = BCMath::from('0');

        $this->assertEquals(0, $b->comp('0'));
        $this->assertEquals(-1, $b->comp('1'));
        $this->assertEquals(1, $b->comp('-1'));
    }

    public function testDiv(): void
    {
        $b = BCMath::from('10');

        $this->assertNotSame($b, $b->div('5'));
        $this->assertEquals('2', $b->div('5')->getValue());
        $this->assertEquals('5', $b->div('2')->getValue());
        $this->assertEquals('2', $b->div('4')->getValue());
        $this->assertEquals('2.5', $b->div('4', '1')->getValue());
    }

    public function testMod(): void
    {
        $b = BCMath::from('50');

        $this->assertNotSame($b, $b->mod('100'));
        $this->assertEquals('24', $b->mod('26')->getValue());
        $this->assertEquals('0', $b->mod('1')->getValue());
        $this->assertEquals('0', $b->mod('25')->getValue());
        $this->assertEquals('20',  $b->mod('30')->getValue());
        $this->assertEquals('10', $b->mod('20')->getValue());
    }

    public function testMul(): void
    {
        $b = BCMath::from('2');

        $this->assertNotSame($b, $b->mul('2'));
        $this->assertEquals('10', $b->mul('5')->getValue());
        $this->assertEquals('7.5', $b->mul('3.75', '1')->getValue());
        $this->assertEquals('22', $b->mul('11')->getValue());
    }

    public function testPow(): void
    {
        $b = BCMath::from('2');

        $this->assertNotSame($b, $b->pow('1'));
        $this->assertEquals('1', $b->pow('0')->getValue());
        $this->assertEquals('2', $b->pow('1')->getValue());
        $this->assertEquals('4', $b->pow('2')->getValue());
        $this->assertEquals('256', $b->pow('8')->getValue());
    }

    public function testPowmod(): void
    {
        $b = BCMath::from('2');

        $this->assertNotSame($b, $b->powmod('1', '5'));
        $this->assertEquals('4', $b->powmod('2', '5')->getValue());
        $this->assertEquals('3', $b->powmod('3', '5')->getValue());
    }

    public function testSqrt(): void
    {
        $b = BCMath::from('1');

        $this->assertNotSame($b, $b->sqrt());
        $this->assertEquals('1.414', BCMath::from('2')->sqrt('3')->getValue());
        $this->assertEquals('1.732', BCMath::from('3')->sqrt('3')->getValue());
    }

    public function testSub(): void
    {
        $b = BCMath::from('10');

        $this->assertNotSame($b, $b->sub('0'));
        $this->assertEquals('10', $b->sub('0')->getValue());
        $this->assertEquals('9', $b->sub('1')->getValue());
        $this->assertEquals('4', $b->sub('6')->getValue());
        $this->assertEquals('0', $b->sub('10')->getValue());
        $this->assertEquals('-2', $b->sub('12')->getValue());
    }

    public function testFloor(): void
    {
        $b = BCMath::from('10.25', 5);
        $floored = $b->floor();

        $this->assertEquals('10', $floored->getValue());
        $this->assertEquals(5, $floored->getScale());

        $b = BCMath::from('-1.3');

        $this->assertEquals('-2', $b->floor()->getValue());
    }

    public function testCeil(): void
    {
        $b = BCMath::from('10.25', 5);
        $ceiled = $b->ceil();

        $this->assertEquals('11', $ceiled->getValue());
        $this->assertEquals(5, $ceiled->getScale());

        $b = BCMath::from('-1.3');

        $this->assertEquals('-1', $b->ceil()->getValue());
    }

    public function testRound(): void
    {
        $b = BCMath::from('3.4', 4);

        $rounded = $b->round();
        $this->assertEquals('3', $rounded->getValue());
        $this->assertEquals(4, $b->getScale());

        $b = BCMath::from('135.79');

        $rounded = $b->round(3);
        $this->assertEquals('135.79', $rounded->getValue());

        $rounded = $b->round(2);
        $this->assertEquals('135.79', $rounded->getValue());

        $rounded = $b->round(1);
        $this->assertEquals('135.8', $rounded->getValue());

        $rounded = $b->round(0);
        $this->assertEquals('136', $rounded->getValue());

        $rounded = $b->round(-1);
        $this->assertEquals('140', $rounded->getValue());

        $rounded = $b->round(-2);
        $this->assertEquals('100', $rounded->getValue());

        $rounded = $b->round(-3);
        $this->assertEquals('0', $rounded->getValue());

        $b = BCMath::from('9.5');

        $this->assertEquals('10', $b->round(0, PHP_ROUND_HALF_UP));
        $this->assertEquals('9', $b->round(0, PHP_ROUND_HALF_DOWN));
        $this->assertEquals('10', $b->round(0, PHP_ROUND_HALF_EVEN));
        $this->assertEquals('9', $b->round(0, PHP_ROUND_HALF_ODD));

        $b = BCMath::from('8.5');

        $this->assertEquals('9', $b->round(0, PHP_ROUND_HALF_UP));
        $this->assertEquals('8', $b->round(0, PHP_ROUND_HALF_DOWN));
        $this->assertEquals('8', $b->round(0, PHP_ROUND_HALF_EVEN));
        $this->assertEquals('9', $b->round(0, PHP_ROUND_HALF_ODD));
    }
}
